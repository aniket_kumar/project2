/**
 * function to return keys in object
 * @param {*} object -given object 
 */
function keys(object){
    if(typeof object !=="object") return [];
    let res=[];
    for(let key in object){
        res.push(key);
    }
    return res;
}
/**
 * returns values of object
 * @param {*} object - given object
 */
function values(object){
    if(typeof object !=="object") return [];
    let res=[];
    for(let key in object){
        res.push(object[key]);
    }
    return res;
}
/**
 * map for object
 * @param {*} object - Given object
 * @param {*} callback = callback function
 */
function mapObject(object,callback){
    if(typeof object !=="object" || typeof callback !=="function") return [];
    let res=[];
    for(let key in object){
        res.push(callback(object[key],key));
    }
    return res;
}
/**
 * Returns [Key,Value] pair
 * @param {*} object - Given object
 */
function pairs(object) {
    if(typeof object !== "object") return [];
    let res=[];
    for(let key in object){
        let temp=[];
        temp.push(key);
        temp.push(object[key]);
        res.push(temp);
    }
    return res;
}

/**
 * Invert the key value pairs.
 * @param {*} object -Given object
 */
function invert(object) {
    if(typeof object !== "object") return {};
    let res={};
    for(let key in object){
        res[object[key]]=key;
    }
    return res;
}
/**
 * function add default value if value is absent
 * @param {*} object -Given object
 * @param {*} def - Default parameter passed
 */
function defaults(object,defaultObject){
    if(typeof defaultObject !== "object" && typeof object !== "object") return {};
    if(typeof object !== "object" && typeof defaultObject === "object") return defaultObject;
    if(typeof object === "object" && typeof defaultObject !=="object") return object;
    
    let defaultArray=[];
    for(let key in defaultObject) defaultArray.push(key);

    let newObject={};
    for(let key in object){
        newObject[key] =object[key];
    }

    let objArr=[];
    for(let key in newObject) objArr.push(key);

    for(let index=0;index<defaultArray.length;index++){
        if(objArr.indexOf(defaultArray[index]) === -1){
            newObject[defaultArray[index]]=defaultObject[defaultArray[index]];
        }
    }
    return newObject;
}
module.exports ={
    keys,
    values,
    mapObject,
    pairs,
    invert,
    defaults
}