/**
 * Return object containing increment and decrement function
 */
function counterFactory() {
  let count = 0;
  const object = {
    increment() {
      return ++count;
    },
    decrement() {
      return --count;
    },
  };
  return object;
}
/**
 * count number of times function is invoked and will return null if invoked more than n times
 * @param {*} callback callback function
 * @param {*} n number of times a function can be invoked
 */
function limitFunctionCount(callback,n) {
    let numberOfTimesFunctionCalled=0;
    if(typeof callback !== "function") return null;

    function count() {
        ++numberOfTimesFunctionCalled;
        if(numberOfTimesFunctionCalled > n) return null;
        return callback();
    }
    return count;

}
/**
 * function return cache object if value passed is present otherwise add value to cache.
 * @param {*} callback -callback function
 */
function cachedFunction(callback) {
    if(typeof callback !== "function") return;
    let cache={};
    function inner(val){
      if(typeof val === undefined) return ;
      if(val in cache){
        return cache;
      }else{
        cache[val]=val;
      }
    }
    return inner;
}
module.exports = { 
    counterFactory,
    limitFunctionCount,
    cachedFunction
};
