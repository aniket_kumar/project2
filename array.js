/**
 * forEach implementation
 * @param {*} arr - required array
 * @param {*} callback -callback function
 */
function each(arr, callback) {

  if (typeof callback !== "function") {
    return "callback function not passed";
  }
  if (Array.isArray(arr)) {
    for (let index = 0; index < arr.length; index++) {
      callback(arr[index], index, arr);
    }
  } else {
    return "Its not array";
  }
}

/**
 * map implemented
 * @param {*} arr - Array input
 * @param {*} callback - callback function
 */
function map(arr, callback) {
  let res = [];
  if (typeof callback !== "function") return [];
  if (Array.isArray(arr)) {
    for (let index = 0; index < arr.length; index++) {
      res.push(callback(arr[index], index, arr));
    }
    return res;
  }
}
/**
 * filter array by given condition of callback
 * @param {*} arr - Given Array
 * @param {*} callback - Callback function
 */
function filter(arr, callback) {
  let res = [];
  if (typeof callback !== "function") return [];
  if (Array.isArray(arr)) {
    for (let index = 0; index < arr.length; index++) {
      if (callback(arr[index], index, arr)) res.push(arr[index]);
    }
    return res;
  } else {
    return [];
  }
}/**
 * find if given present in array
 * @param {*} arr - Given Array
 * @param {*} callback - Callback function
 */
function find(arr, callback) {
  if (typeof callback !== "function") return undefined;
  if (Array.isArray(arr)) {
    for (let index = 0; index < arr.length; index++) {
      if (callback(arr[index], index, arr)) return true;
    }
  } else {
    return undefined;
  }
  return false;
}

/**
 * this function return flatten array
 * @param {*} element 
 */
function flatten(element) {
  let res = [];
  if (Array.isArray(element)) {
    for (let index = 0; index < element.length; index++) {
      if (Array.isArray(element[index])) {
        res = res.concat(flatten(element[index]));
      } else {
        res.push(element[index]);
      }
    }
    return res;
  } else {
    return res;
  }
}

/**
 * Reduce function implemented
 * @param {*} arr - Array of element
 * @param {*} callback - callback function
 * @param {*} startValue - starting value
 */
function reduce(arr, callback, startValue) {
  let startIndex=0
  if (startValue === undefined){
      startValue =1;
      startValue = arr[0];
  }
  if (Array.isArray(arr) && typeof callback === "function") {
    for (let index = startIndex; index < arr.length; index++) {
      startValue = callback(startValue, arr[index]);
    }
    return startValue;
  } else return "Incorrect argument passed..";
}

module.exports ={
    each,
    map,
    filter,
    find,
    flatten,
    reduce
}